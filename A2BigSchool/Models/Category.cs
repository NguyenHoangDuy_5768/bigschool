﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace A2BigSchool.Models
{
    public class Category
    {
            public byte Id { get; set; }
         
            
            public string Name { get; set; }
    }
}